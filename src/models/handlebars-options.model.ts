import { IsArray, IsObject, IsOptional, IsString } from 'class-validator';
import * as validPath from 'is-valid-path';
import { validFile } from '../lib/util';
import { IsTrue } from '../lib/validators/is-true.validator';

/**
 * The data model assigned to Handlebars tasks
 */
export class HandlebarsOptions {
  @IsString()
  @IsTrue(validFile)
  public readonly input: string;
  @IsString()
  @IsTrue(validPath)
  public readonly output: string;
  @IsOptional()
  @IsObject()
  public readonly context?: any = {};
  @IsOptional()
  @IsArray()
  @IsString({
    each: true
  })
  @IsTrue(validFile, {
    each: true
  })
  public readonly files?: readonly string[];

  public constructor(private readonly options: HandlebarsOptions) {
    this.input = this.options.input;
    this.output = this.options.output;
    this.context = this.options.context;
    this.files = this.options.files;
  }
}
