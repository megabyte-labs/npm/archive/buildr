import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { validFile } from '../lib/util';
import { IsTrue } from '../lib/validators/is-true.validator';

/**
 * The data model assigned to JSON markdown tasks
 */
export class JSONMarkdownOptions {
  @IsString()
  @IsTrue(validFile)
  /**
   * The input file. This should be a .md file that you wish to inject into
   * a JSON file.
   */
  public readonly source: string;
  @IsString()
  @IsTrue(validFile)
  /**
   * This is the target JSON file
   */
  public readonly target: string;
  @IsString()
  public readonly key: string;
  @IsOptional()
  @IsBoolean()
  /** Whether or not to translate the the  */
  public readonly translate?: boolean;

  public constructor(private readonly options: JSONMarkdownOptions) {
    this.key = this.options.key;
    this.source = this.options.source;
    this.target = this.options.target;
    this.translate = this.options.translate;
  }
}
