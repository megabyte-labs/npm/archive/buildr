import { Logger } from '../lib/log';

/**
 * The data model assigned to scenes
 */
export class SceneOptions {
  public constructor(private readonly options: SceneOptions) {
    Logger.log(this.options);
  }
}
