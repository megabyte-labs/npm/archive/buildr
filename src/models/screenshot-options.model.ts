// Import { IsNumber, IsOptional, IsString, Validator } from 'class-validator';
import { IsNumber, IsOptional, IsString } from 'class-validator';
import * as validPath from 'is-valid-path';
// Import { validFile } from '../lib/util'; TODO
import { IsTrue } from '../lib/validators/is-true.validator';

// Const validator = new Validator(); TODO

const defaultScreenshotHeight = 720;
const defaultScreenshotWidth = 1280;

/**
 * The data model assigned to each screenshot
 */
export class ScreenshotOptions {
  @IsOptional()
  @IsNumber()
  /** Height of the screenshot to be taken */
  public readonly height?: number = defaultScreenshotHeight;
  @IsString()
  // @IsTrue((o) => validator.isFQDN(o) || validFile(o)) TODO
  /**
   * The input file path or URL that puppeteer will open to take a
   * screenshot of
   */
  public readonly input: string;
  @IsString()
  @IsTrue(validPath)
  /**
   * The output file path. Should end in .png
   */
  public readonly output: string;
  @IsOptional()
  @IsNumber()
  /** The width of the screenshot to be taken */
  public readonly width?: number = defaultScreenshotWidth;

  public constructor(private readonly options: ScreenshotOptions) {
    this.height = this.options.height;
    this.input = this.options.input;
    this.output = this.options.output;
    this.width = this.options.width;
  }
}
