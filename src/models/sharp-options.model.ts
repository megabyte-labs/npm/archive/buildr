import { Logger } from '../lib/log';

/**
 * The data model assigned to sharp tasks
 */
export class SharpOptions {
  public constructor(private readonly options: SharpOptions) {
    Logger.log(this.options);
  }
}
