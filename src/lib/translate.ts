import * as fs from 'fs';
import * as path from 'path';
import * as dotenv from 'dotenv';
dotenv.config();
import languageNameMap from 'language-name-map/map';
import { TranslationsOptions } from '../models/translations-options.model';
import { Logger } from './log';
import { createDir, diffObjects, dirFiles, format, writeFile } from './util';

/**
 * Details on Translate found here:
 * [@google-cloud/translate](https://www.npmjs.com/package/@google-cloud/translate)
 */
const { Translate } = require('@google-cloud/translate').v2; // eslint-disable-line

/** An object with the details of the file currently being processed */
interface FileDetails {
  /** The language code */
  readonly lang: string;
  /** The path to the file currently being translated */
  readonly path: string;
}

/**
 * Uses [@google-cloud/translate](https://www.npmjs.com/package/@google-cloud/translate)
 * to scan an i18n folder and inject translations that are missing when compared to
 * the base translation.
 *
 * Use the service by passing the [[TranslateServiceConfig]] to a new instance of TranslateService
 * like this:
 * ```
 * const taintedTranslate = new TranslateService(translateServiceConfig);
 * taintedTranslate.run();
 * ```
 */
export class TranslateService {
  private readonly config: TranslationsOptions;
  private readonly googleTranslate: any;

  public constructor(private readonly translateConfig: TranslationsOptions) {
    this.config = this.translateConfig;
    this.googleTranslate = new Translate({ projectId: this.translateConfig.projectId });
  }

  /**
   * Runs the translation routine using the configuration passed into the constructor
   */
  public async run(): Promise<void> {
    await Promise.all(this.config.source.map(async (x) => this.runReference(x)));
  }

  /**
   * This first acquires an array of all the file paths and corresponding languages. Then
   * it creates folders/empty files for any targets that do not already have data. And then
   * it acquires the missing translations.
   *
   * @param reference The path to the source file currently being processed
   * @returns A promise that resolves when the translation process is complete
   */
  private async runReference(reference: string): Promise<void> {
    try {
      const isDir = fs.existsSync(reference) && fs.lstatSync(reference).isDirectory();
      const files = isDir ? await getDirectoryFiles(reference) : await getJSONFiles(reference);
      const baseFiles = files.result.filter((x) => x.lang === files.referenceLanguage);
      const languages = this.config.languages ? this.config.languages : [...new Set(files.result.map((x) => x.lang))];
      if (isDir) {
        // Initialize empty folders if it is a directory type configuration
        await createEmptyFolders(reference, languages);
      }
      // Initialize empty files for missing translations
      await Promise.all(baseFiles.map(async (x) => createEmptyFiles(reference, languages, x, isDir)));
      try {
        await Promise.all(baseFiles.map(async (x) => this.acquireMissingTranslations(x, languages, isDir)));
      } catch (e) {
        Logger.error('Failed to acquire translations', e);
        Logger.error(e);
      }
    } catch (e) {
      Logger.error('Failed to acquire existing file data and create empty base files', e);
    }
  }

  /**
   * Compares each of the i18n files to the reference language file and then uses Google
   * Translate to inject missing translations.
   *
   * @param baseFile The [[FileDetails]] of the reference file
   * @param files An array of languages to translate the file to
   * @param isDir Whether or not it is a directory of translations
   * @returns A promise that resolves when the translation process is complete for a given baseFile
   */
  private async acquireMissingTranslations(
    baseFile: FileDetails,
    languages: readonly string[],
    isDir: boolean
  ): Promise<void> {
    try {
      const base: {} = require(baseFile.path); // eslint-disable-line
      const translations = languages.map((x) => ({
        lang: x,
        output: {},
        path: isDir
          ? `${path.dirname(path.dirname(baseFile.path))}/${x}/${path.basename(baseFile.path)}`
          : `${path.dirname(baseFile.path)}/${x}.json`
      }));
      await Promise.all(
        translations.map(async (translation) => {
          const lang = translation.lang;
          const _path = translation.path; // eslint-disable-line no-underscore-dangle
          // Load the translations
          const original = require(_path); // eslint-disable-line
          const diff = diffObjects(original, base); // eslint-disable-line
          const updates = await this.getTranslations(diff, lang);
          if (updates.translated) {
            const data = format(JSON.stringify({ ...updates.results, ...original }), { parser: 'json' });
            await writeFile(_path, data);
          }
        })
      );
    } catch (e) {
      if (e && e.code === '400') {
        Logger.error('400 error from Google Translate');
        Logger.warn(
          'Your environment is probably not set up correctly. You must set the \
        GOOGLE_APPLICATION_CREDENTIALS environment variable. See the GitHub page for more \
        details.'
        );
      } else {
        Logger.error('Failed to acquire missing translations', e);
      }
    }
  }

  /**
   * A recursive function that crawls through an i18n file and adds missing translations
   * to the file as well as an indicator of whether or not any translations took place.
   *
   * @param obj An object with the current state of the i18n file being translated
   * @param lang The two character language string
   * @returns An object containing the translated i18n file as well as an indicator of
   * whether or any translations took place
   */
  private async getTranslations(
    obj: any,
    lang: string
  ): Promise<{ readonly results: {}; readonly translated: boolean }> {
    const results = {};
    let translated = false;
    for (const key of Object.keys(obj)) {
      const item = obj[key];
      if (typeof item === 'string') {
        // String to be translated
        const displayLanguage = languageNameMap[lang] ? languageNameMap[lang].name : lang;
        Logger.info(`Translating "${item}" to ${displayLanguage}`);
        const [translation] = await this.googleTranslate.translate(item, lang);
        results[key] = translation;
        translated = true;
      } else {
        // Is a nested object
        const val = await this.getTranslations(item, lang);
        if (val.translated) {
          translated = true;
        }
        results[key] = val.results;
      }
    }

    // Will never get here
    return { results, translated };
  }
}

/**
 * This creates empty files for any targets that do not exist
 *
 * @param reference The path to the source file currently being processed
 * @param languages An array of language codes
 * @param baseFile Details on the es.json|fr.json or file in the translatable directory
 * @param isDir True if the translation routine type is a directory translation
 * @returns A promise that resolves when all the empty files are created
 */
async function createEmptyFiles(
  reference: string,
  languages: readonly string[],
  baseFile: FileDetails,
  isDir: boolean
): Promise<void> {
  await Promise.all(
    languages.map((language) => {
      const fileToCheck = isDir
        ? `${path.dirname(reference)}/${language}/${path.basename(baseFile.path)}`
        : `${path.dirname(reference)}/${language}.json`;

      return !fs.existsSync(fileToCheck) ? writeFile(fileToCheck, '{}') : null;
    })
  );
}
/**
 * If the reference type is a directory, then this will create empty folders for any
 * of the (optional) languages passed in.
 *
 * @param reference The path to the source file currently being processed
 * @param languages An array of language codes
 * @returns A promise that resolves when all the folders have been created
 */
async function createEmptyFolders(reference: string, languages: readonly string[]): Promise<void> {
  await Promise.all(
    languages.map((language) => {
      const folder = `${path.dirname(reference)}/${language}`;

      return !fs.existsSync(folder) ? createDir(folder) : null;
    })
  );
}
/**
 * This assumes the directory name is a language code (e.g. "en"). It then scans through all of the different
 * folders that are named language codes and returns an array containing objects with both
 * the language and the path.
 *
 * @param reference The path to the source file currently being processed
 * @returns A promise that resolves with an array of objects containing the language and path of each
 * i18n file.
 */
async function getDirectoryFiles(
  reference: string
): Promise<{
  readonly result: readonly FileDetails[];
  readonly referenceLanguage: string;
}> {
  const directories = await dirFiles(path.dirname(reference));
  const filtered = directories.filter((x) => fs.existsSync(x) && fs.lstatSync(x).isDirectory());
  const files = await Promise.all(filtered.map(async (x) => dirFiles(x)));
  const validFile = new RegExp('.json$');
  const filteredFiles = files.filter((x) => validFile.test(x.toString()));
  const result = (filtered as any)
    .map((_path: string, index: number) =>
      filteredFiles[index].map((file) => ({
        lang: path.basename(_path),
        path: file
      }))
    )
    .flat();

  return { result, referenceLanguage: path.basename(reference) };
}
/**
 * This assumes that the target file is named "en.json", for instance. It then acquires all
 * the other files in the same folder and assumes they are also named {{LanguageCode}}.json.
 *
 * @param reference The path to the source file currently being processed
 * @returns A promise that resolves when the translation process is complete
 */
async function getJSONFiles(
  reference: string
): Promise<{
  readonly result: readonly FileDetails[];
  readonly referenceLanguage: string;
}> {
  const referenceLanguage = path.basename(reference).replace('.json', '');
  const files = await dirFiles(path.dirname(reference));
  const validFile = new RegExp('.json$');
  const filtered = files.filter((x) => validFile.test(x));
  const result = filtered.map((x) => ({
    lang: path.basename(x).replace('.json', ''),
    path: x
  }));

  return { result, referenceLanguage };
}
