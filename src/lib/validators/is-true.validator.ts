/* eslint-disable @typescript-eslint/naming-convention */
import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';

/**
 * TODO
 *
 * @param property
 * @param validationOptions
 */
export function IsTrue(property: any, validationOptions?: ValidationOptions): Function {
  return (object: object, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      name: 'IsTrue',
      options: validationOptions,
      constraints: [property],
      validator: {
        validate(value: any, args: ValidationArguments): any {
          if (typeof args.constraints[0] === 'function') {
            return !!args.constraints[0].apply(null, [value]);
          }
        }
      }
    });
  };
}
